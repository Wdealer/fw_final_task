#include "command_parser.h"

#include "uart_driver.h"
#include "main.h"
#include <stdio.h>
#include <string.h>

extern uart_driver_t uart_driver;
extern float Temp_DegC;
extern uint32_t fan_duty;
extern uint8_t door;
uint8_t alarm = 0;


uint8_t fan_status = 0;
uint8_t heater = 0;
int x = 1;
int y = 0;
int z = 0;
char temperatura_in_str[7];
char version_in_str[14];


const uint8_t ack_msg[] = "*ACK#";
const uint8_t nack_msg[] = "*NACK#";
const uint8_t fan_0_msg[] = "*F000#";
const uint8_t fan_25_msg[] = "*F025#";
const uint8_t fan_50_msg[] = "*F050#";
const uint8_t fan_75_msg[] = "*F075#";
const uint8_t fan_100_msg[] = "*F100#";
const uint8_t off_fan_msg[] = "*0F#";
const uint8_t on_fan_msg[] = "*#1F";
const uint8_t off_alarm_msg[] = "*0A#";
const uint8_t on_alarm_msg[] = "*1A#";
const uint8_t door_open_msg[] = "*D0#";
const uint8_t door_closed_msg[] = "*D1#";
const uint8_t heater_on[] = "*H1#";
const uint8_t heater_off[] = "*H0#";


const uint8_t read_temperature[] = "TEMPERATURE";
const uint8_t read_fan[] = "READ_FAN";
const uint8_t fan_0[] = "FAN_IN_0";
const uint8_t fan_25[] = "FAN_IN_25";
const uint8_t fan_50[] = "FAN_IN_50";
const uint8_t fan_75[] = "FAN_IN_75";
const uint8_t fan_100[] = "FAN_IN_100";
const uint8_t door_open[] = "DOOR_OPEN";
const uint8_t door_closed[] = "DOOR_CLOSED";
const uint8_t door_read[] = "READING_DOOR";
const uint8_t heater_read[] = "READING_HEATER";
const uint8_t fw_request[] = "FW_REQUEST";
const uint8_t report_request[] = "REPORT_REQUEST";




void print_temperatura (void){

	temperatura();
	int Temperatura_entera = (int) (Temp_DegC);
	int Temperatura_decimal = (int) (Temp_DegC*100);
	Temperatura_decimal = Temperatura_decimal % 100;

	if (Temperatura_decimal > 10){
		sprintf(temperatura_in_str, "*T%d.%d#", Temperatura_entera,Temperatura_decimal);

	} else {
		sprintf(temperatura_in_str, "*T%d.0%d#", Temperatura_entera,Temperatura_decimal);

	}

	uart_driver_send(&uart_driver, (uint8_t *)temperatura_in_str, sizeof(temperatura_in_str));

}

void print_fan_duty (void){

	if (fan_duty == 0){
		uart_driver_send(&uart_driver, (uint8_t *)fan_0_msg, sizeof(fan_0_msg)-1);
	} else if (fan_duty == 250){
		uart_driver_send(&uart_driver, (uint8_t *)fan_25_msg, sizeof(fan_25_msg)-1);
	} else if (fan_duty == 500){
		uart_driver_send(&uart_driver, (uint8_t *)fan_50_msg, sizeof(fan_50_msg)-1);
	} else if (fan_duty == 750){
		uart_driver_send(&uart_driver, (uint8_t *)fan_75_msg, sizeof(fan_75_msg)-1);
	} else if (fan_duty == 1000){
		uart_driver_send(&uart_driver, (uint8_t *)fan_100_msg, sizeof(fan_100_msg)-1);
	}

}

void print_fan_status (void){
	if (fan_duty > 0){
		uart_driver_send(&uart_driver, (uint8_t *)on_fan_msg, sizeof(on_fan_msg)-1);
	} else {
		uart_driver_send(&uart_driver, (uint8_t *)off_fan_msg, sizeof(off_fan_msg)-1);
	}
}

void print_door_status (void){
	if (door == 1){
		uart_driver_send(&uart_driver, (uint8_t *)door_closed_msg, sizeof(door_closed_msg)-1);
	} else {
		uart_driver_send(&uart_driver, (uint8_t *)door_open_msg, sizeof(door_open_msg)-1);
	}
}

void print_heater_status (void){
	if (Temp_DegC < 12){
		heater = 1;
		uart_driver_send(&uart_driver, (uint8_t *)heater_on, sizeof(heater_on)-1);
	} else{
		heater = 0;
		uart_driver_send(&uart_driver, (uint8_t *)heater_off, sizeof(heater_off)-1);

	}
}

void print_alarm_status (void){
	if (Temp_DegC < 0){
		alarm = 1;
		uart_driver_send(&uart_driver, (uint8_t *)on_alarm_msg, sizeof(on_alarm_msg)-1);

	} else{
		alarm = 0;
		uart_driver_send(&uart_driver, (uint8_t *)off_alarm_msg, sizeof(off_alarm_msg)-1);

	}
}

void parse_command(uint8_t *rx_packet)
{

	if (memcmp(rx_packet, read_temperature, sizeof(read_temperature)-1) == 0) {
		print_temperatura();
	}

	else if (memcmp(rx_packet, read_fan, sizeof(read_fan)-1) == 0) {
		print_fan_duty();

	} else if (memcmp(rx_packet, fan_0, sizeof(fan_0)-1) == 0) {
		fan_duty = 0;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);

	} else if (memcmp(rx_packet, fan_25, sizeof(fan_25)-1) == 0) {
		fan_duty = 250;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);

	} else if (memcmp(rx_packet, fan_50, sizeof(fan_50)-1) == 0) {
		fan_duty = 500;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);

	} else if (memcmp(rx_packet, fan_75, sizeof(fan_75)-1) == 0) {
		fan_duty = 750;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);

	} else if (memcmp(rx_packet, fan_100, sizeof(fan_100)-1) == 0) {
		fan_duty = 1000;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);
	}

	else if (memcmp(rx_packet, door_open, sizeof(door_open)-1) == 0) {
		door = 0;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);

	} else if (memcmp(rx_packet, door_closed, sizeof(door_closed)-1) == 0) {
		door = 1;
		uart_driver_send(&uart_driver, (uint8_t *)ack_msg, sizeof(ack_msg)-1);


	} else if (memcmp(rx_packet, door_read, sizeof(door_read)-1) == 0) {
		print_door_status();
	}


	else if (memcmp(rx_packet, heater_read, sizeof(heater_read)-1) == 0) {
		print_heater_status();

	}

	else if (memcmp(rx_packet, fw_request, sizeof(fw_request)-1) == 0) {
		z = z+1;
		if (z == 100000000){
			z = 0;
			y = y + 1;
		}
		if (y == 10){
			y = 0;
			x = x + 1;
		}
		if (x == 10){
			x = 1;
			y = 0;
			x = 0;
		}
		if (z >= 10000000){
			sprintf(version_in_str, "*%d.%d.%d#", x, y, z);

		} else if (z >= 1000000){
			sprintf(version_in_str, "*%d.%d.0%d#", x, y,z);
		} else if (z >= 100000){
			sprintf(version_in_str, "*%d.%d.00%d#", x, y,z);
		} else if (z >= 10000){
			sprintf(version_in_str, "*%d.%d.000%d#", x, y,z);
		} else if (z >= 1000){
			sprintf(version_in_str, "*%d.%d.0000%d#", x, y,z);
		} else if (z >= 100){
			sprintf(version_in_str, "*%d.%d.00000%d#", x, y,z);
		} else if (z >= 10){
			sprintf(version_in_str, "*%d.%d.000000%d#", x, y,z);
		} else {
			sprintf(version_in_str, "*%d.%d.0000000%d#", x, y,z);
		}
		uart_driver_send(&uart_driver, (uint8_t *)version_in_str, sizeof(version_in_str));

	}

	else if (memcmp(rx_packet, report_request, sizeof(report_request)-1) == 0) {
		z = z+1;
		if (z == 100000000){
			z = 0;
			y = y + 1;
		}
		if (y == 10){
			y = 0;
			x = x + 1;
		}
		if (x == 10){
			x = 1;
			y = 0;
			x = 0;
		}


		print_fan_status();
		print_heater_status();
		print_door_status();
		print_alarm_status();
		print_temperatura();
	}

	// - - - Read anything else - - - //
	else {

		uart_driver_send(&uart_driver, (uint8_t *)nack_msg, sizeof(nack_msg)-1);


	}
}
