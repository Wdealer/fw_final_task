Titoma Firmware Final Task

Jhon Alejandro Sanchez Sabaria 
C.C: 1053873573
alsanchezsa@unal.edu.co

In this code, a protocol is created using UART based on the "Command Parser". 
Through this protocol the STM32 communicates with the ESP32, 
followed by the connection with the Titoma's mqtt.

This project uses a BMP280 module 
by which it measures the temperature periodically and communicates via SPI.


This code has the following UART commands:

Read Temperature
Read Fan Speed
Close Door
Open Door
Get Door Status
Get the FW (Updated every time a relase is made)
Get Heater status
Invalid Command (Typing error or command not defined)